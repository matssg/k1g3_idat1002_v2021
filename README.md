![ToDo List](/images/banner.png)
<p>Standalone Java To-Do List that allow you to effortlessly keep track of everything that you need to do.</p>

<p align="left">
  <a href="https://gitlab.stud.idi.ntnu.no/matssg/k1g3_idat1002_v2021/-/wikis/home">Wiki</a> • 
  <a href="https://gitlab.stud.idi.ntnu.no/matssg/k1g3_idat1002_v2021/-/boards">Issue board</a> • 
  <a href="http://matssg.pages.stud.idi.ntnu.no/k1g3_idat1002_v2021/allpackages-index.html">Javadoc</a> • 
  <a href="https://gitlab.stud.idi.ntnu.no/matssg/k1g3_idat1002_v2021/-/tree/master">Source Code</a>
</p><br>

![ToDo List](/images/screenshot.png)

# Features
- **List.** Add, remove, edit and view tasks in an easy to use list.
- **Plan.** Add near or distant deadlines so you can schedule accordingly.
- **Portable.** Keep the application on your computer or a memory stick for easy access.

# Getting started
Download the application from our [releases page](https://gitlab.stud.idi.ntnu.no/matssg/k1g3_idat1002_v2021/-/releases).
<details><summary>Requirements</summary>

- Java Runtime Environment (JRE) version 15 or higher
</details>

# About
_Cohort 1 - Group 3_
<p>Project assignment given as part of NTNU Trondheim's course IDATT1002.</p>
<details><summary>Contributors</summary>

- Kristoffer Høiaas Aandahl
- Mats Solem Gravem
- Maamoun Adnan Hmaidoush
- Oskar Langås Eidem
</details>
