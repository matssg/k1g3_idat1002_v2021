package no.ntnu.idatt;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class TaskTest {
    private final Task test1 = new Task("Win a game of chess", 3, LocalDateTime.of(2013, 11, 22, 12, 10));
    private final Task test2 = new Task("pass the Hunter Examination", "A yearly event which an applicant must pass in order to become a Hunter, an elite member of humanity.", 1, LocalDateTime.of(1999, 12, 31, 14, 30));
    private final Task test3 = new Task("Defuse thE BoMb", "Prevent Terrorists from bombing the nuclear reactor.", 2, LocalDateTime.of(2021, 10, 20, 11, 15), LocalDateTime.of(2020, 9, 22, 10, 30), false);

    @Test
    public void testGetLabelInLowerCase() {
        Assertions.assertEquals("win a game of chess", test1.getLabelInLowerCase());
        Assertions.assertEquals("defuse the bomb", test3.getLabelInLowerCase());
    }

    @Test
    public void testSetPriority() {
        Assertions.assertEquals(1, test2.getPriority());
        Assertions.assertTrue(test2.setPriority(2));
        Assertions.assertEquals(2, test2.getPriority());
    }

    @Test
    public void testEquals_True() {
        Task test = new Task("Defuse thE BoMb", "Prevent Terrorists from bombing the nuclear reactor.", 2, LocalDateTime.of(2021, 10, 20, 11, 15), LocalDateTime.of(2020, 9, 22, 10, 30), false);
        Assertions.assertEquals(test , test3);
        Assertions.assertTrue(test3.equals(test));
    }
}