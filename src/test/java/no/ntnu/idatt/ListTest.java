package no.ntnu.idatt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ListTest {
    private final List toDoList = new List("toDoList");
    private final List expectedToDoList = new List("expectedToDoList");

    private final Task test1 = new Task("Win a game of chess", 3, LocalDateTime.of(2013, 11, 22, 12, 10));
    private final Task test2 = new Task("pass the Hunter Examination", "A yearly event which an applicant must pass in order to become a Hunter, an elite member of humanity.", 1, LocalDateTime.of(1999, 12, 31, 14, 30));
    private final Task test3 = new Task("Defuse the bomb", "Prevent Terrorists from bombing the nuclear reactor.", 2, LocalDateTime.of(2021, 10, 20, 11, 15), LocalDateTime.of(2020, 9, 22, 10, 30), false);

    @Nested
    public class _testGet {

        public void setUp() {
            toDoList.addTask(test1);
            toDoList.addTask(test2);
            toDoList.addTask(test3);
        }

        @Test
        public void all_tasks() {
            setUp();

            ArrayList<Task> expected = new ArrayList<>();
            expected.add(test1);
            expected.add(test2);
            expected.add(test3);
            Assertions.assertEquals(expected, toDoList.getAllTasks());
        }

        @Test
        public void non_completed_tasks() {
            setUp();

            test3.setCompleted(true);
            expectedToDoList.addTask(test1);
            expectedToDoList.addTask(test2);
            Assertions.assertEquals(expectedToDoList.getAllTasks(), toDoList.getNonCompletedTasks());
        }

        @Test
        public void completed_tasks() {
            setUp();

            test3.setCompleted(true);
            expectedToDoList.addTask(test3);
            Assertions.assertEquals(expectedToDoList.getAllTasks(), toDoList.getCompletedTasks());
        }
    }

    @Nested
    public class _testAddTask {

        @Test
        public void single_task() {
            toDoList.addTask(test1);
            Assertions.assertEquals(test1, toDoList.getAllTasks().get(0));
        }

        @Test
        public void multiple_tasks() {
            toDoList.addTask(test1);
            toDoList.addTask(test2);
            Assertions.assertEquals(2, toDoList.getAllTasks().size());
        }
    }

    @Nested
    public class _testRemove {

        @Test
        public void existing_task() throws RemoveException {
            toDoList.addTask(test1);
            toDoList.remove(test1);
        }

        @Test
        public void nonexistent_task() {
            toDoList.addTask(test1);
            Assertions.assertThrows(RemoveException.class, () -> toDoList.remove(test3));
        }
    }

    @Nested
    public class _testGetSortedTasks {

        @Test
        public void by_dueDate() {
            toDoList.addTask(test1); toDoList.addTask(test2); toDoList.addTask(test3);
            toDoList.getSortedTasks(1);
            expectedToDoList.addTask(test2); expectedToDoList.addTask(test1); expectedToDoList.addTask(test3);

            Assertions.assertEquals(expectedToDoList.getAllTasks(), toDoList.getAllTasks());
        }

        @Test
        public void by_priority() {
            toDoList.addTask(test1); toDoList.addTask(test2); toDoList.addTask(test3);
            toDoList.getSortedTasks(2);
            expectedToDoList.addTask(test2); expectedToDoList.addTask(test3); expectedToDoList.addTask(test1);
            
            Assertions.assertEquals((expectedToDoList.getAllTasks()), toDoList.getAllTasks());
        }

        @Test
        public void by_label() {
            toDoList.addTask(test1); toDoList.addTask(test2); toDoList.addTask(test3);
            toDoList.getSortedTasks(3);
            expectedToDoList.addTask(test3); expectedToDoList.addTask(test2); expectedToDoList.addTask(test1);

            Assertions.assertEquals((expectedToDoList.getAllTasks()), toDoList.getAllTasks());
        }
    }
}
