package no.ntnu.idatt.filehandler;

import no.ntnu.idatt.List;
import no.ntnu.idatt.Task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.time.LocalDateTime;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SaveFileTest {
    private final SaveFile testFileTask = new SaveFile("test.task");
    private final SaveFile testFileList = new SaveFile("test.list");

    private final List toDoList = new List("toDoList");

    private final Task test1 = new Task("Win a game of chess", 3, LocalDateTime.of(2013, 11, 22, 12, 10));
    private final Task test2 = new Task("pass the Hunter Examination", 1, LocalDateTime.of(1999, 12, 31, 14, 30));
    private final Task test3 = new Task("Defuse the bomb", 2, LocalDateTime.of(2020, 04, 01, 12, 30));

    @AfterAll
    public void cleanUp() {
        testFileTask.delete();
        testFileList.delete();
    }

    @Test
    public void testDelete_True() {
        Assertions.assertTrue(testFileTask.delete());
    }

    @Nested
    public class task{

        @Test
        public void testDoSerialize() {
            boolean success = true;
            try {
                testFileTask.doSerialize(test1);
            } catch (IOException e) {
                success = false;
            }
            Assertions.assertTrue(success);
        }

        @Test
        public void testDoDeSerialize () throws IOException, ClassNotFoundException {
            testDoSerialize();
            Task imported = (Task) testFileTask.doDeserialize();
            Assertions.assertEquals(test1, imported);
        }
    }

    @Nested
    public class _list {

        public void setUp() {
            toDoList.addTask(test1);
            toDoList.addTask(test2);
            toDoList.addTask(test3);
        }

        @Test
        public void testDoSerialize() {
            setUp();
            boolean success = true;
            try {
                testFileList.doSerialize(toDoList);
            } catch (IOException e) {
                success = false;
            }
            Assertions.assertTrue(success);
        }

        @Test
        public void testDoDeSerialize() throws IOException, ClassNotFoundException {
            testDoSerialize();
            List imported = (List) testFileList.doDeserialize();
            Assertions.assertEquals(toDoList, imported);
        }

        @Test
        public void testIsAToDoList_False() throws IOException {
            testFileTask.doSerialize(test1);
            Assertions.assertFalse(testFileTask.isAToDoList());
        }

        @Test
        public void testIsAToDoList_True() throws IOException {
            testFileList.doSerialize(toDoList);
            Assertions.assertTrue(testFileList.isAToDoList());
        }
    }
}
