module no.ntnu.idatt {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt.controller to javafx.fxml;
    exports no.ntnu.idatt;
}