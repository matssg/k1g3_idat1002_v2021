package no.ntnu.idatt.controller;

import no.ntnu.idatt.*;
import no.ntnu.idatt.filehandler.SaveFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

/**
 * The controller for {@code main.fxml}.
 *<p>
 * The controller contains current main {@link List} and methods for buttons/functionality.
 */

public class MainController {
    @FXML private StackPane stack;
    // ▼ Main view
    @FXML private HBox topBar;
    @FXML private VBox taskList;
    // ▼ Main menu
    @FXML private AnchorPane listMenuLayer;
    @FXML private ImageView completedTaskMarker;
    @FXML private ImageView dueDateMarker;
    @FXML private ImageView priorityMarker;
    @FXML private ImageView NameMarker;
    // ▼ List management
    @FXML private AnchorPane chooseListLayer;
    @FXML private AnchorPane newListLayer;
    @FXML private Label newListLabel;
    @FXML private TextField newListName;
    @FXML private ComboBox<String> listSelector;
    // ▼ Task add/edit menu
    @FXML private Pane newTaskFormLayer;
    @FXML private Label newTaskMainLabel;
    @FXML private Label newNameLabel;
    @FXML private TextField newTaskName;
    @FXML private TextArea newTaskDescription;
    @FXML private Label newDateLabel;
    @FXML private DatePicker newTaskDueDate;
    @FXML private Label newTimeLabel;
    @FXML private TextField newTaskDueTime;
    @FXML private Label newPriorityLabel;
    @FXML private RadioButton newTaskPrioHigh;
    @FXML private RadioButton newTaskPrioMedium;
    @FXML private RadioButton newTaskPrioLow;
    @FXML private Button commitNewTaskButton;
    // ▼ Extended task view
    @FXML private AnchorPane taskExtendedLayer;
    @FXML private AnchorPane taskExtendedMenuLayer;
    @FXML private Label taskExtendedName;
    @FXML private Label taskExtendedDate;
    @FXML private Label taskExtendedPriority;
    @FXML private Label taskExtendedDateAdded;
    @FXML private TextFlow taskExtendedDescription;
    // ▼ Launch window for no save file
    @FXML public AnchorPane launchWithOutLists;
    @FXML public Label launchListLabel;
    @FXML public TextField launchListName;

    private DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("H:mm");
    private ArrayList<List> lists = new ArrayList<>();
    private ArrayList<SaveFile> saveFiles = new ArrayList<>();
    private List list;
    private Task selectedTask;
    private ToggleGroup selectPriority;
    private Boolean isEditing = false;

    // ▼ Default Settings ▼
    private Boolean showCompletedTasks = false;
    private int sort = 1;
    // ▲ Default settings ▲

    /**
     * Automatically called to initialize a controller after its root element has been completely processed.
     */
    public void initialize() throws IOException, ClassNotFoundException {
        newTaskDueDate.setShowWeekNumbers(false);

        // Drag undecorated window by topBar
        topBar.setOnMousePressed(pressEvent -> topBar.setOnMouseDragged(dragEvent -> {
            App.primaryStage.setX(dragEvent.getScreenX() - pressEvent.getSceneX());
            App.primaryStage.setY(dragEvent.getScreenY() - pressEvent.getSceneY());
        }));

        // Close menu and pop-up by clicking outside
        listMenuLayer.setOnMouseClicked(mouseEvent -> listMenuLayer.toBack());

        // Initialising the select priority radiobutton group
        selectPriority = new ToggleGroup();

        newTaskPrioLow.setToggleGroup(selectPriority);
        newTaskPrioMedium.setToggleGroup(selectPriority);
        newTaskPrioHigh.setToggleGroup(selectPriority);

        fillListArray();
        if (lists.size() == 0){
            launchWithOutLists.toFront();
        } else {
            // Fills the list selector and the list array
            fillListSelector();
            list = lists.get(0);

            // Adds tasks to main list
            sortByDate();
        }
    }

    //region Getters
    public ArrayList<List> getLists() {
        return lists;
    }

    public ArrayList<SaveFile> getSaveFiles() {
        return saveFiles;
    }

    public List getList() {
        return list;
    }
    //endregion

    //region Validators
    private boolean isValidDate(String date) {
        try {
            LocalDate.parse(date, formatterDate);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    private boolean isValidTime(String time) {
        try {
            LocalTime.parse(time, formatterTime);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }
    //endregion

    //region Tasklist manipulators
    /**
     * Refresh main list of tasks
     */
    public void refactorList() {
        // Remove all tasks
        taskList.getChildren().clear();

        // Add tasks to list again based on settings
        if (!showCompletedTasks) {
            addTasksToTaskList(list.getNonCompletedTasks());
        } else {
            addTasksToTaskList(list.getNonCompletedTasks());
            addTasksToTaskList(list.getCompletedTasks());
        }
    }

    public void sortList() {
        if (sort == 1) {sortByDate();}
        else if (sort == 2) {sortByPriority();}
        else if (sort == 3) {sortByName();}
    }

    public void sortByDate() {
        sort = 1;
        dueDateMarker.setVisible(true);
        priorityMarker.setVisible(false);
        NameMarker.setVisible(false);

        list.getSortedTasks(1);
        refactorList();
    }

    public void sortByPriority() {
        sort = 2;
        dueDateMarker.setVisible(false);
        priorityMarker.setVisible(true);
        NameMarker.setVisible(false);

        list.getSortedTasks(2);
        refactorList();
    }

    public void sortByName() {
        sort = 3;
        dueDateMarker.setVisible(false);
        priorityMarker.setVisible(false);
        NameMarker.setVisible(true);

        list.getSortedTasks(3);
        refactorList();
    }

    public void toggleCompletedTasks() {
        if (showCompletedTasks) {
            completedTaskMarker.setVisible(false);
            showCompletedTasks = false;
        } else {
            completedTaskMarker.setVisible(true);
            showCompletedTasks = true;
        }
        refactorList();
    }

    /**
     * Adds every task in a list of tasks to the main screen
     * @param tasks
     */
    public void addTasksToTaskList(ArrayList<Task> tasks) {
        tasks.forEach(task -> {
            // Set task layout and define variables
            FXMLLoader taskBoxFXML = new FXMLLoader(getClass().getResource("/fxml/task.fxml"));
            StackPane taskElement = null;

            // Load taskBoxFXML as an HBox element
            try {taskElement = taskBoxFXML.load();} 
            catch (IOException exception) {exception.printStackTrace();}

            // Fill in information per task
            TaskController taskController = taskBoxFXML.getController();
            taskController.setMainController(this);

            taskController.initialize(task);

            // Add taskElement to the VBox list
            taskList.getChildren().add(taskElement);
        });
    }

    public void showTaskExtendedLayer(Task target, StackPane root) {
        //Searches for the index of the root
        int flag = -1;
        for (int i = 0; i < taskList.getChildren().size(); i++) {
            if (taskList.getChildren().get(i) == root){
                flag = i;
            }
        }
        refactorList();


        FXMLLoader taskBoxFXML = new FXMLLoader(getClass().getResource("/fxml/extendedTask.fxml"));
        StackPane taskElement = null;


        // Load taskBoxFXML as an HBox element
        try {taskElement = taskBoxFXML.load();}
        catch (IOException exception) {exception.printStackTrace();}
        ExtendedTaskController taskController = taskBoxFXML.getController();
        taskController.initialize(target);
        taskController.setMainController(this);

        //Closes previous open extended views
        taskList.getChildren().remove(flag,flag+1);

        taskList.getChildren().add(flag, taskElement);
    }
    //endregion

    //region Topbar events
    /**
     * Method to close application
     * @param event
     */
    public void handleCloseButtonAction(ActionEvent event) throws IOException, ClassNotFoundException {
        for (int i = 0; i<saveFiles.size(); i++) {
            saveFiles.get(i).doSerialize(lists.get(i));
        }
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Method to minimize application to the taskbar
     * @param event
     */
    public void handleMinimizeButtonAction(ActionEvent event) {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }
    //endregion

    //region Add or edit tasks
    public void addTask() {
        // Reset all previous inputs
        newTaskName.setText("");
        newTaskDescription.setText("");
        newTaskDueDate.setValue(null);
        newTaskDueTime.setText("");
        try {
            selectPriority.getSelectedToggle().setSelected(false);
        } catch (Exception e) {}

        resetNewTaskLabels();

        // Specifies that the form is not editing a task
        isEditing = false;

        // Bring 'New Task' menu to the front
        newTaskFormLayer.toFront();
    }

    public void editTask(Task task) {
        selectedTask = task;
        // Imports input field values
        newTaskName.setText(task.getLabel());
        newTaskDescription.setText(task.getDescription());
        newTaskDueDate.setValue(task.getDueDate().toLocalDate());
        newTaskDueTime.setText(task.getDueDate().format(formatterTime));

        if (task.getPriority() == 1) {
            newTaskPrioHigh.setSelected(true);
        }
        else if (task.getPriority() == 2) {
            newTaskPrioMedium.setSelected(true);
        }
        else if (task.getPriority() == 3) {
            newTaskPrioLow.setSelected(true);
        }

        resetNewTaskLabels();
        newTaskMainLabel.setText("Edit Task");
        commitNewTaskButton.setText("Save");

        isEditing = true;

        newTaskFormLayer.toFront();
    }

    public void commitNewTask() {
        // Variables to store values
        int priority = 0;
        LocalDateTime dueDate;
        String name = newTaskName.getText().trim();
        String description = newTaskDescription.getText();
        String dateFromTextField = newTaskDueDate.getEditor().getText();
        String timeFromTextField = newTaskDueTime.getText();

        // Get priority value from RadioButtons
        RadioButton selected = (RadioButton) selectPriority.getSelectedToggle();
        if (newTaskPrioLow.equals(selected)) priority = 3;
        else if (newTaskPrioMedium.equals(selected)) priority = 2;
        else if (newTaskPrioHigh.equals(selected)) priority = 1;

        // Check if all fields are filled in
        if (name.equals("") || priority == 0 || !isValidDate(dateFromTextField) || !isValidTime(timeFromTextField)) {
            newTaskMainLabel.setText("Please fill in all required fields");
            newTaskMainLabel.setTextFill(Color.RED);
            if (name.equals("")) newNameLabel.setTextFill(Color.RED);
            if (priority == 0) newPriorityLabel.setTextFill(Color.RED);
            if (!isValidDate(dateFromTextField)) {
                newDateLabel.setTextFill(Color.RED);
                newTaskDueDate.getEditor().setText("");
            }
            if (!isValidTime(timeFromTextField)) {
                newTimeLabel.setTextFill(Color.RED);
                newTaskDueTime.setText("");
            }
        }
        // If all fields are filled in -> create new task object
        else if (isEditing) {
                newDateLabel.setTextFill(Color.WHITE);
                newTimeLabel.setTextFill(Color.WHITE);
                dueDate = LocalDateTime.of(LocalDate.parse(dateFromTextField, formatterDate), LocalTime.parse(timeFromTextField, formatterTime));
                selectedTask.setLabel(name);
                selectedTask.setDescription(description);
                selectedTask.setPriority(priority);
                selectedTask.setDueDate(dueDate);
                newTaskFormLayer.toBack();
        }
        else {
            dueDate = LocalDateTime.of(LocalDate.parse(dateFromTextField, formatterDate), LocalTime.parse(timeFromTextField, formatterTime));
            if (description.equals("")) {
                list.addTask(new Task(name, priority, dueDate));
            }
            else {
                list.addTask(new Task(name, description, priority, dueDate));
            }
            newTaskDueDate.getEditor().setText(""); // resets text box if invalid input last try.
            newTaskDueTime.setText("");

            newTaskFormLayer.toBack();
        }
        sortList();
    }

    public void closeNewTaskMenu() {
        newTaskFormLayer.toBack();
    }

    public void resetNewTaskLabels() {
        newTaskMainLabel.setText("New Task");
        commitNewTaskButton.setText("Add");
        newTaskMainLabel.setTextFill(Color.WHITE);
        newNameLabel.setTextFill(Color.rgb(220,220,220));
        newPriorityLabel.setTextFill(Color.rgb(220,220,220));
        newDateLabel.setTextFill(Color.rgb(220,220,220));
        newTimeLabel.setTextFill(Color.rgb(220,220,220));
    }
    //endregion

    //region Adding or selecting to do list
    public void showListMenu() {
        listMenuLayer.toFront();
    }

    public void selectList() {
        for (List l : lists) {
            if (l.getLABEL().equals(listSelector.getValue())) {
                list = l;
                sortList();
            }
        }
        closeChooseListMenu();
    }

    public void fillListArray() throws IOException, ClassNotFoundException {
        saveFiles.clear();
        lists.clear();
        File saveFileDir = new File(SaveFile.SAVE_DIRECTORY);
        String[] saveFileStrings= saveFileDir.list();

        assert saveFileStrings != null;
        for (String name : saveFileStrings) {
            if (name.endsWith(".sav")) {
                File file = new File(SaveFile.SAVE_DIRECTORY + name);
                SaveFile saveFile = new SaveFile(file);
                if (saveFile.isAToDoList()) {
                    lists.add((List) saveFile.doDeserialize());
                    saveFiles.add(saveFile);
                }
            }
        }
    }

    public void fillListSelector() {
        listSelector.getItems().clear();

        try {fillListArray();}
        catch (IOException | ClassNotFoundException e) {e.printStackTrace();}

        for (List list : lists) {
            listSelector.getItems().add(list.getLABEL());
        }
        listSelector.setValue(lists.get(0).getLABEL());
    }

    public void closeNewListMenu() {
        newListLayer.toBack();
    }

    public void closeChooseListMenu() {
        chooseListLayer.toBack();
    }

    public void addNewList() {
        boolean exists = false;
        for (String name : listSelector.getItems()) {
            if (name.equals(newListName.getText())) {
                exists = true;
                break;
            }
        }

        if (!newListName.getText().trim().equals("") && !exists) {
            List list = new List(newListName.getText());
            lists.add(list);
            saveFiles.add(new SaveFile(list.getLABEL()));

            listSelector.getItems().add(list.getLABEL());
            listSelector.setValue(list.getLABEL());

            this.list = list;
            refactorList();
            newListLayer.toBack();
        } else {
            newListLabel.setTextFill(Color.RED);
            newListLabel.setText("In use");
        }
    }

    public void addList() {
        newListName.setText("");
        newListLabel.setText("Name:");
        newListLabel.setTextFill(Color.rgb(220,220,220));
        newListLayer.toFront();
    }

    public void launchNewList() {
        if (!launchListName.getText().trim().equals("")) {
            List list = new List(launchListName.getText());
            lists.add(list);
            saveFiles.add(new SaveFile(list.getLABEL()));

            listSelector.getItems().add(list.getLABEL());
            listSelector.setValue(list.getLABEL());

            this.list = list;
            refactorList();
            launchWithOutLists.toBack();
        } else {
            launchListLabel.setTextFill(Color.RED);
        }

    }
    //endregion

    public void handleChooseListButton() {
        chooseListLayer.toFront();
    }
}
