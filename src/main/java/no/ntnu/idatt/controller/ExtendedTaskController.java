package no.ntnu.idatt.controller;

import no.ntnu.idatt.Task;
import no.ntnu.idatt.RemoveException;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * The controller for {@code extendedTask.fxml}.
 */
public class ExtendedTaskController extends TaskController {
    @FXML private TextFlow taskExtendedDescription;
    @FXML private Label taskExtendedPriority;
    @FXML private Label taskExtendedDateAdded;

    /**
     * Fills information into the extended view
     *
     * @param task the view represents
     */
    @Override
    public void initialize(Task task) {
        this.task = task;

        // Fill fields with information
        label.setText(task.getLabel());
        dueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm", Locale.US)));

        taskExtendedDescription.getChildren().clear();
        Text text;
        if (task.getDescription() == null || task.getDescription().equals("")) {
            text = new Text("Nothing here!\nAdd a description while editing a task!");
        } else {
            text = new Text(task.getDescription());
        }
        text.getStyleClass().add("descriptionText");
        taskExtendedDescription.getChildren().add(text);

        switch (task.getPriority()) {
            case 1: 
                taskExtendedPriority.setText("High");
                taskExtendedPriority.setTextFill(Color.rgb(200, 90, 90));
                break;
            case 2: 
                taskExtendedPriority.setText("Medium");
                taskExtendedPriority.setTextFill(Color.rgb(200, 200, 90));
                break;
            case 3: 
                taskExtendedPriority.setText("Low");
                taskExtendedPriority.setTextFill(Color.rgb(90, 200, 90));
                break;
            default: 
                break;}

        taskExtendedDateAdded.setText(task.getAddedDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.US)));
        completed(task.isCompleted(), task.getPriority());
    }

    /**
     * Closes the extended view
     *
     * @param mouseEvent
     */
    public void collapseAccordion(MouseEvent mouseEvent) {
        mainController.refactorList();
    }

    /**
     * The method is called then clicking the edit task button, and opens the menu for editing the selected task
     *
     * @param actionEvent
     */
    public void editTask(ActionEvent actionEvent) {
        mainController.editTask(task);
    }

    /**
     * The method is called then clicking the delete task button and removes the task from the currently selected list
     *
     * @param actionEvent
     * @throws RemoveException
     */
    public void deleteTask(ActionEvent actionEvent) throws RemoveException {
        mainController.getList().remove(task);
        mainController.refactorList();
    }
}
