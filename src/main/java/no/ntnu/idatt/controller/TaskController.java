package no.ntnu.idatt.controller;

import no.ntnu.idatt.Task;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * The controller for {@code task.fxml}.
 * It is a superclass of the {@link ExtendedTaskController}.
 *<p>
 * The controller contains methods to edit tasks and to change the view style
 */
public class TaskController {
    @FXML protected StackPane taskRoot;
    @FXML protected Label label;
    @FXML protected Label dueDate;
    @FXML protected Button checkButton;

    protected Task task;
    protected MainController mainController;

    /**
     * Fills the element with information
     *
     * @param task the task the element gets information from
     */
    public void initialize(Task task) {
        this.task = task;
        label.setText(task.getLabel());
        dueDate.setText(task.getDueDate().toString());
        setDueDate(task);
        completed(task.isCompleted(), task.getPriority());
    }


    /**
     * Allows the task controller to use methods from the main controller
     *
     * @param mainController
     */
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
    public void setLabel(String label) {
        this.label.setText(label);
    }

    /**
     * Sets the due date label, it also formats the label depending on the due dates relation to the current date
     *
     * @param task element to get the duedate from
     */
    public void setDueDate(Task task) {
        if (task.getDueDate().getYear() != LocalDate.now().getYear()) {
            this.dueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.US)));
        } else if (task.getDueDate().toLocalDate().equals(LocalDate.now())) {
            this.dueDate.setText("Today");
        } else if (task.getDueDate().toLocalDate().equals(LocalDate.now().plusDays(1))) {
            this.dueDate.setText("Tomorrow");
        } else if (task.getDueDate().toLocalDate().minusDays(7).isBefore(LocalDate.now()) && task.getDueDate().toLocalDate().isAfter(LocalDate.now())) {
            this.dueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("EEEE", Locale.US)));
        } else {
            this.dueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("dd MMMM HH:mm", Locale.US)));
        }

    }
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * Change style of button depending on completion status and priority
     * @param completed 
     * @param priority
     */
    public void completed(boolean completed, int priority) {
        checkButton.getStyleClass().clear();

        if (completed) {
            checkButton.getStyleClass().add("priority" + priority);
            checkButton.getStyleClass().add("checkButtonCompleted");
            label.setOpacity(0.4);
            dueDate.setOpacity(0.2);
        } else {
            checkButton.getStyleClass().add("checkButton");
            checkButton.getStyleClass().add("priority" + priority);
        }
    }

    /**
     * Changes completion status and style of button when pressed
     */
    public void handleCheckButtonAction() {
        if (!task.isCompleted()) {
            task.setCompleted(true);
            completed(true, task.getPriority());
        } else {
            task.setCompleted(false);
            completed(false, task.getPriority());
        }
        mainController.sortList();
    }

    /**
     * Shows extended view of task
     */
    public void showTaskExtendedLayer() {
        mainController.showTaskExtendedLayer(task, taskRoot);
    }
}
