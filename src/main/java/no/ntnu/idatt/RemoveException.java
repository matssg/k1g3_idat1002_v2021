package no.ntnu.idatt;

/**
 * Checked exception that throws if item is not removed.
 * <p>
 * Checked exceptions need to be declared in a method or constructor's throws 
 * clause if they can be thrown by the execution of the method or constructor and propagate 
 * outside the method or constructor boundary.
 */
public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String message){
        super("Could not reomve task: " + message);
    }
}
