package no.ntnu.idatt.filehandler;

import no.ntnu.idatt.List;
import no.ntnu.idatt.Task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Save file class:
 * <ul>
 *      <li>Creates a persistent {@code saveFile}.</li>
 *      <li>Writes an object to the {@code saveFile}.</li>
 *      <li>Returns the object from the {@code saveFile}.</li>
 *      <li>Deletes the {@code saveFile}.</li>
 * </ul>
 */
public class SaveFile {
    /**
     * Path for save folder
     */
    public static final String DIRECTORY = System.getProperty("user.dir") + "/ToDoList";
    /**
     * Directory path where save files are stored
     */
    public static final String SAVE_DIRECTORY = DIRECTORY + "/";
    /**
     * File extention for save files
     */
    public static final String EXTENSION = ".sav";

    private final File saveFile;

    /**
     * Generates a new {@link File} to store data to in correct directory and correct name convention
     * @param fileName the name of the new save file
    */
    public SaveFile(String fileName) {
        // Try to remake save directory just in case
        File saveDir = new File(DIRECTORY);
        saveDir.mkdirs();

        String target = SAVE_DIRECTORY + fileName + EXTENSION;
        saveFile = new File(target);
        try {
            if (!saveFile.createNewFile()) {
                System.out.println(fileName + " Already exists");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public SaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    /**
     * Serializes an object and writes to the {@code saveFile}
      * @param obj
      * @throws IOException Any exception thrown by the underlying OutputStream.
     */
    public void doSerialize(Object obj) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(saveFile));
        out.writeObject(obj);
        out.close();
    }

    /**
     * Deletes the {@code saveFile}
     * @return {@code true} the file got deleted;
     *         {@code false} if not
     */
    public boolean delete() {
        return saveFile.delete();
    }


    /**
     * Checks if a file contains an object which can be cast to list, and that the list only contains tasks
     * @return {@code true} if the save file contains a legitimate instance of a to do list;
     *         {@code false} if not
     */
    public boolean isAToDoList() {
        List list;
        try {
            list = (List) doDeserialize();
            for (int i = 0; i< list.getAllTasks().size(); i++) {
                if (!(list.getAllTasks().get(i) instanceof Task)) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    /**
     * Deserializes the {@code saveFile} and returns the values of the deserialized object
     * @return value of the serialized object, needs to be casted to correct object type
     * @throws IOException Any of the usual Input/Output related exceptions.
     * @throws ClassNotFoundException Class of a serialized object cannot be found.
     */
    public Object doDeserialize() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(saveFile));
        Object obj = in.readObject();

        in.close();
        return obj;
    }
}
