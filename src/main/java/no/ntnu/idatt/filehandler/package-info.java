/**
 * Contains classes concerning reading and writing files as well as serialization.
 */
package no.ntnu.idatt.filehandler;