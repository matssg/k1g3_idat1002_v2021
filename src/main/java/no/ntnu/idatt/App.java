package no.ntnu.idatt;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import no.ntnu.idatt.controller.MainController;

import java.io.File;
import java.io.IOException;

/**
 * JavaFX App, this is the main class of the application.
 * Loads FXML files and shows stages.
 */

public class App extends Application {

    private static Scene scene;
    public static Stage primaryStage;
    StackPane mainElement = null;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("To-Do List");
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/icon.png")));

        scene = new Scene(loadFXML("main"));
        stage.setScene(scene);
        primaryStage = stage;
        stage.show();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    @Override
    public void stop() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/main.fxml"));

        // Load taskBoxFXML as an HBox element
        try {mainElement = fxmlLoader.load();}
        catch (IOException exception) {exception.printStackTrace();}
        MainController mainController = fxmlLoader.getController();

        for (int i = 0; i < mainController.getSaveFiles().size(); i++) {
            mainController.getSaveFiles().get(i).doSerialize(mainController.getLists().get(i));
        }
    }

    public static void main(String[] args) {
        File saveDir = new File(System.getProperty("user.dir") + "/ToDoList");
        saveDir.mkdirs();

        launch();
    }
}