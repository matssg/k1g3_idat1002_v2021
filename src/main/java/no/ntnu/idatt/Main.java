package no.ntnu.idatt;

/**
 * Launcher class, its purpose is to launch the program through {@code App.java}.
 */

public class Main {
    public static void main(String[] args) {
        App.main(args);
    }
}
