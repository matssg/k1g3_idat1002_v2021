package no.ntnu.idatt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * {@code List} represents list of tasks.
 * {@code List} is a mutable and serializable class.
 */

public class List implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String LABEL;
	private ArrayList<Task> tasks;

    public List(String label) {
	    this.LABEL = label;
        tasks = new ArrayList<>();
    }

    public String getLABEL() {
        return LABEL;
    }

    // Get-methods
    public ArrayList<Task> getAllTasks() {
        return tasks;
    }
    public ArrayList<Task> getNonCompletedTasks() {
        return (ArrayList<Task>) tasks.stream().filter(t -> !t.isCompleted()).collect(Collectors.toList());
    }
    public ArrayList<Task> getCompletedTasks() {
        return (ArrayList<Task>) tasks.stream().filter(Task::isCompleted).collect(Collectors.toList());
    }

    /**
     * Returns a sorted list.
     * @param sortBy {@code int} representing sorting type (1 = by date, 2 = by priority, 3 = alphabetically)
     */
    public void getSortedTasks(int sortBy) {
        switch (sortBy) {
            case 1: 
                this.tasks.sort(Comparator.comparing(Task::getDueDate));
                break;
            case 2:
                this.tasks.sort(Comparator.comparingInt(Task::getPriority));
                break;
            case 3:
                this.tasks.sort(Comparator.comparing(Task::getLabelInLowerCase));
                break;
            default:
                break;
        }
    }

    // Set-methods
    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    /**
     * Removes given {@code Task} from list
     * @param task The task which should be removed
     * @throws RemoveException if task is not removed
     */
    public void remove(Task task) throws RemoveException {
        if (!tasks.contains(task)) {                                    // Checks if list contains task
            throw new RemoveException("Task does not exist in list");
        } else {
            boolean result = tasks.remove(task);                        // Tries to remove task removed the casting to task as the method already only accepts tasks
            if (!result) {
                throw new RemoveException("Task not registered");
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}

        List that = (List) o;
        return tasks.equals(that.tasks);
    }
    
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("ToDo List: ");
        for(Task task : tasks) {
            string.append("\n\t").append(task.getLabel()).append(", ").append(task.getDueDate());
        }
        return string.toString();
    }
}
