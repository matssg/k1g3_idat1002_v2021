package no.ntnu.idatt;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * {@code Task} represents a single task yet to be completed
 * {@code Task} is a mutable and serializable class.
 */

public class Task implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * A short description or the name of the task
     */
    private String label;
    /**
     * An optional longer description of the task
     */
    private String description;
    /**
     * A {@code int} representing the importance of the task, lower means more important
     */
    private int priority = 0;
    /**
     * The due date of the task
     */
    private LocalDateTime dueDate;
    /**
     * The date {@code Task} was created
     */
    private final LocalDateTime addedDate;
    /**
     * Whether or not the task is completed
     */
    private boolean completed;

    /**
     * Constructor for importing {@code Task} objects from backup.
     * Initializes all attributes.
     *
     * @param label The main description of the task
     * @param description Longer description of task
     * @param priority The importance of the task
     * @param addedDate When the task was added
     * @param dueDate When the task is due
     * @param completed If the task is completed or not
     */
    public Task(String label, String description, int priority, LocalDateTime dueDate, LocalDateTime addedDate, boolean completed) {
        this.label = label;
        this.description = description;
        setPriority(priority);
        this.dueDate = dueDate;
        this.addedDate = addedDate;
        this.completed = completed;
    }

    /**
     * Constructor for adding new task without description
     *
     * @param label The main description of the task
     * @param priority The importance of the task
     * @param dueDate When the task is due
     */
    public Task(String label, int priority, LocalDateTime dueDate) {
        this.label = label;
        this.description = null;
        setPriority(priority);
        this.dueDate = dueDate;
        this.addedDate = LocalDateTime.now();
        this.completed = false;
    }

    /**
     * Constructor for adding new task with description
     *
     * @param label The main description of the task
     * @param description A brief description of the task
     * @param priority The importance of the task
     * @param dueDate When the task is due
     */
    public Task(String label, String description, int priority, LocalDateTime dueDate) {
        this.label = label;
        this.description = description;
        setPriority(priority);
        this.dueDate = dueDate;
        this.addedDate = LocalDateTime.now();
        this.completed = false;
    }

    // Get-methods
    public String getLabel() {
        return label;
    }

    /**
     * Gets the name of the task in all lower case
     * @return the name in lower case
     */
    public String getLabelInLowerCase() {
        return label.toLowerCase();
    }
    public String getDescription() {
        return description;
    }
    public int getPriority() {
        return priority;
    }
    public LocalDateTime getDueDate() {
        return dueDate;
    }
    public LocalDateTime getAddedDate() {
        return addedDate;
    }
    public boolean isCompleted() {
        return completed;
    }

    // Set-methods
    public void setLabel(String label) {
        this.label = label;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Sets the priority of the task. 
     * Only changes the value if input is in range 0 to 4
     * @param priority the priority of the task, lower has higher priority
     * @return {@code true} if the priority was changed;
     *         {@code false} if not
     */
    public boolean setPriority(int priority) {
        if (priority<=3 && priority>=1) {
            this.priority = priority;
            return true;
        }
        return false;
    }
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return priority == task.priority &&
                completed == task.completed &&
                Objects.equals(label, task.label) &&
                Objects.equals(description, task.description) &&
                Objects.equals(dueDate, task.dueDate) &&
                Objects.equals(addedDate, task.addedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, description, priority, dueDate, addedDate, completed);
    }

    @Override
    public String toString() {
        return  label + "\n Description: " + description + 
                "\n Priority: " + priority + 
                "\n Date added: " + addedDate + 
                "\n Due date: " + dueDate + 
                "\n Status completion: " + completed;
    }
}
